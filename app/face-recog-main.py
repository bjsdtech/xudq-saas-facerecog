# 导入 FastAPI
from fastapi import FastAPI

import uvicorn

from app.apis.faces_recog_api import face_recog_router

# 生成 FastAPI 类实例
saas_face_recog_app = FastAPI()


@saas_face_recog_app.get('/')
async def home() -> dict:
    return {
        "code": "000000",
        "msg": "欢迎使用数动人脸识别服务，目前支持图片、视频、摄像等（单人/多人）人脸识别和人脸比对。",
        "result: ": ""
    }


@saas_face_recog_app.post('/')
async def home() -> dict:
    return {
        "code": "000000",
        "msg": "欢迎使用数动人脸识别服务，目前支持图片、视频、摄像等（单人/多人）人脸识别和人脸比对。",
        "result: ": ""
    }

saas_face_recog_app.include_router(face_recog_router)

if __name__ == '__main__':
    uvicorn.run(saas_face_recog_app, host='127.0.0.1', port=8083, log_level="info")
