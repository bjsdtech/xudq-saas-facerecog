# -*- coding: utf-8 -*-
import pymysql
import configparser
from sshtunnel import SSHTunnelForwarder

conf = configparser.ConfigParser()
conf.read('conf/saas_setting.conf', encoding='gbk')

ssh = (conf.get("mysql", 'ssh'))

ssh_host = conf.get("mysql", 'ssh_host')
ssh_port = int(conf.get("mysql", 'ssh_port'))
ssh_username = conf.get("mysql", 'ssh_username')
ssh_password = conf.get("mysql", 'ssh_password')

mysql_host = conf.get("mysql", 'mysql_host')
mysql_port = int(conf.get("mysql", 'mysql_port'))

mysql_username = conf.get("mysql", 'mysql_username')
mysql_password = conf.get("mysql", 'mysql_password')

mysql_db = conf.get('mysql', 'mysql_database')
mysql_charset = conf.get("mysql", 'mysql_charset')


def init_connect():

    if "YES" == ssh:
        server = SSHTunnelForwarder(
            ssh_address_or_host=(ssh_host, ssh_port),
            ssh_username=ssh_username,
            ssh_password=ssh_password,
            remote_bind_address=(mysql_host, mysql_port)
        )
        server.start()
        conn = pymysql.connect(
            host="127.0.0.1",
            port=server.local_bind_port,
            user=mysql_username,
            passwd=mysql_password,
            db=mysql_db,
            charset=mysql_charset)

        return conn
    elif "NO" == ssh:
        conn = pymysql.connect(
            host=mysql_host,  # 数据库的IP地址
            user=mysql_username,  # 数据库用户名称
            password=mysql_password,  # 数据库用户密码
            db=mysql_db,  # 数据库名称
            port=mysql_port,  # 数据库端口名称
            charset=mysql_charset  # 数据库的编码方式
        )
        return conn
    else:
        return None


def execute_sql_with_boolen(sql_str, args=()):

    result = True
    conn = init_connect()

    if conn is None:
        return False

    cursor = conn.cursor()

    try:
        cursor.execute(sql_str, args)
        conn.commit()
    except Exception as e:
        result = False
        conn.rollback()

    conn.close()
    cursor.close()
    return result

def execute_sql_with_boolen(sql_str, args=()):
    conn = init_connect()
    cursor = conn.cursor()
    try:
        cursor.execute(sql_str, args)
        conn.commit()
        return True
    except Exception as e:
        conn.rollback()
        print(e)
        return False
    finally:
        cursor.close()


def create_table_transaction():
    return execute_sql_with_boolen('''
        CREATE TABLE IF NOT EXISTS `''' + mysql_db + '''`.`t_transaction`  (
        `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
        `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除状态：0-未删除，1-冻结，2-删除',
        `trans_id` varchar(64) NOT NULL DEFAULT '' COMMENT 'SaaS服务内部交易ID',
        `user_id` varchar(32) NOT NULL DEFAULT '' COMMENT '用户ID，免费交易时用户IDSaaS服务固定设置',
        `account_id` varchar(32) NOT NULL DEFAULT '' COMMENT '用户对应的账号ID',
        `request_id` varchar(128) NOT NULL DEFAULT '' COMMENT '外部请求ID',
        `raw_file_name` varchar(64) NOT NULL DEFAULT '' COMMENT '文件名',
        `raw_file_path` varchar(128) NOT NULL DEFAULT '' COMMENT '文件存放地址',
        `rst_file_name` varchar(64) NOT NULL DEFAULT '',
        `rst_file_path` varchar(128) NOT NULL DEFAULT '',
        `service_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '服务ID，1-人脸检测，2-人脸识别',
        `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '处理状态：0-未开始，1-处理中，2-处理失败，4-处理成功',
        `file_type` tinyint(4) NOT NULL DEFAULT '11' COMMENT '文件类型：11-图片，12-证照，13-票证，31-音频，41-视频',
        `fee_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '费用类型：0-免费，1-优惠，3-全额',
        `from_channel` int(11) NOT NULL DEFAULT '0' COMMENT '交易来源,11 - API ',
        `fail_reason` varchar(128) NOT NULL DEFAULT '' COMMENT '失败原因',
        `last_modify_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '',
        `reserve_int_1` int(11) DEFAULT '0' COMMENT '备用字段',
        `reserve_char_1` varchar(128) DEFAULT '' COMMENT '备用字段',
        PRIMARY KEY (`id`,`trans_id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=119 DEFAULT CHARSET=utf8;
''')

def create_table_init_images():
    return execute_sql_with_boolen('''
        CREATE TABLE IF NOT EXISTS `''' + mysql_db + '''`.`t_face_recog_init_image`  (
        `id` bigint(20) NOT NULL AUTO_INCREMENT,
        `is_deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除状态：0-未删除，1-冻结，2-删除',
        `personnel_number` varchar(20) NOT NULL DEFAULT '' COMMENT '人员编号',
        `cn_name` varchar(32) DEFAULT '' COMMENT '人员中文名字',
        `en_name` varchar(32) DEFAULT '' COMMENT '人员英文名字',
        `raw_image_name` varchar(32) NOT NULL DEFAULT '' COMMENT '人脸识别图片名',
        `raw_image_path` varchar(64) NOT NULL DEFAULT '' COMMENT '人脸识别图片目录',
        `from_channel` int(11) NOT NULL DEFAULT '11' COMMENT '图片来源,11 - API ',
        `face_encoding` varchar(4096) NOT NULL DEFAULT '' COMMENT '人脸编码',
        `last_modify_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '最后修改时间',
        `reserve_int_1` int(11) DEFAULT '0' COMMENT '备用字段',
        `reserve_char_1` varchar(128) DEFAULT '' COMMENT '备用字段',
        PRIMARY KEY (`id`)
        ) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
''')

if __name__ == '__main__':
    create_table_transaction()
    create_table_init_images()



