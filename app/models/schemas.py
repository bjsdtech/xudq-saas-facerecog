from typing import Union  # ,List


from pydantic import BaseModel


# 创建初始 Pydantic模型/模式
class InitImageInputRequest(BaseModel):
    picType: str
    transId: str
    timestamp: str
    nonce: str
    secretId: Union[str, None] = None
    signature: Union[str, None] = None


# 创建初始 Pydantic模型/模式
class TransactionBase(BaseModel):
    trans_id: str
    user_id: str
    account_id: Union[str, None] = None
    request_id: str
    raw_file_name: str
    raw_file_path: str
    rst_file_name: str
    rst_file_path: str
    fail_reason: Union[str, None] = None
    reserve_char_1: Union[str, None] = None
