# coding: utf-8
# 导入 FastAPI
from fastapi import APIRouter, UploadFile  # , Response
from fastapi.responses import StreamingResponse
import os
import cv2
import numpy as np
from typing import Union
from enum import Enum
import base64
import hashlib
import time

from PIL import Image, ImageFont, ImageDraw
import face_recognition
import configparser

from app.models.models import FaceRecogInitImage, Transaction
from app.models.crud import get_trans_by_trans_id
from app.models.crud import query_all_init_image

from app.db.database import get_db

from app.util.base import float_array_to_string
from app.util.base import string_to_float_array

from app.commons.logger import logger
from app.commons.common import write_file
from app.commons.common import transid_generator
from app.commons.common import image_type_check
from app.commons.result_code import ERROR_CODE
from app.commons.response_result import response_result
from app.commons.response_result import init_image_return
from app.commons.response_result import face_recog_return

# -从配置文件获取数据库配置信息
# --获取当前文件目录
current_path = os.path.abspath(os.path.dirname(__file__))

# --获取SaaS服务根目录
saas_root_path = current_path[:current_path.rindex('api')]

# --数据库配置文件以及数据操作相关配置项（Linux/macOS）
CONF_DB_FILE = saas_root_path + '/conf/saas_setting.conf'

conf = configparser.ConfigParser()
conf.read(CONF_DB_FILE, encoding='gbk')

TOLERANCE = float(conf.get('const', 'tolerance'))
SETSIZE = float(conf.get('const', 'setsize'))

DATA_FILE_PATH = conf.get('image_conf', 'image_path')

SRCS_PATH = conf.get('resources', 'path')
CASCADE = conf.get('resources', 'cascade')
FONT = conf.get('resources', 'font')
SERVICE_TYPE_IMAGE = '21'
SERVICE_TYPE_VIDEO = '22'
SERVICE_TYPE_CAPTURE = '23'

SECRET_KEY = "123321"

SAVE_INIT_RAW_IMAGE_FILE_PATH = DATA_FILE_PATH+'/raw/recog/images/init/'
SAVE_RAW_RECOG_IMAGE_FILE_PATH = DATA_FILE_PATH+'/raw/recog/images/'
SAVE_RST_RECOG_IMAGE_FILE_PATH = DATA_FILE_PATH+'/rst/recog/images/'

FONT_FILE = SRCS_PATH+FONT

# 这个是对图片转码用的
JPEG_HEADER = "data:image/jpeg;base64,"


class PicType(str, Enum):
    url = "URL"
    base64 = "BASE64"


# create router
face_recog_router = APIRouter(
    prefix='/{version}/facerecog',
    tags=['version']
)


def version_check(version: str):
    if "v1" == version:
        return True
    else:
        return False


@face_recog_router.post("/image/input")
async def initImageInput(version: str, transid: str, imagetype: str, personnum: str, cnname: str, enname: str,
                           timestamp: str, nonce: str, secretid: str = None, file: Union[UploadFile, str] = None) -> dict:
    """
    - author1:
    - version: v1.0
    - title: 已知人脸图片及信息录入
    - description:
    - date:
    - param:
      - version: 本版号，此接口只支持v1
      - tansid: 任务id
      - imagetype: 图片类型：URL，BASE64
      - personnum: 人员编号，比如员工编号
      - cnname: 中文姓名
      - enname: 英文姓名
      - timestamp: 时间戳
      - nonce: 随机码，6位数字
      - secretid: 秘钥ID
      - file: 已知人脸照片，必须是单人清晰正面照
    - return: JSON格式识别结果
    """

    logger.info("(transid:"+transid+") face image input start.")
    logger.info("para:{" +
                "version:" + version +
                ", transid:" + transid +
                ", imagetype:" + imagetype +
                ", personnum:" + personnum +
                ", cnname:" + cnname +
                ", enname:" + enname +
                ", timestamp:" + timestamp +
                ", nonce:" + nonce + "}")

    # 接口版本号校验
    if not version_check(version):
        logger.error("version para error")
        logger.info("transid:" + transid + ")face image input  end.")
        return init_image_return(ERROR_CODE.FD_RECOG_ERROR_VAR_NO_SUPPORT.value, "version must be v1", transid,  status=2)

    # 接口安全校验
    if secretid is not None and secretid.strip():

        if not timestamp.strip():
            logger.error("timestamp para is empty.")
            logger.info("transid:" + transid + ")face image input end.")
            return init_image_return(ERROR_CODE.FD_RECOG_ERROR_TIMESTAMP_EMPTY.value, "timestamp para is empty.",
                                     transid, status=4)
        if not nonce.strip():
            logger.error("nonce para is empty.")
            logger.info("transid:" + transid + ")face image input end.")
            return init_image_return(ERROR_CODE.FD_RECOG_ERROR_NONCE_EMPTY.value, "nonce para is empty.", transid,
                                     status=4)

        secret_raw = timestamp + nonce + SECRET_KEY
        secret_md5 = hashlib.md5(secret_raw.encode()).hexdigest()

        if secret_md5 != secretid:
            logger.error("secret check is failure.")
            logger.info("transid:" + transid + ")face image input end.")
            return init_image_return(ERROR_CODE.FD_RECOG_SECRETID_FAIL.value, "secretid check is failure.", transid,
                                     status=4)
    if not transid.strip():
        logger.error("transid para is empty.")
        logger.info("transid:" + transid + ")face image input end.")
        return init_image_return(ERROR_CODE.FD_RECOG_ERROR_TRANSID_EMPTY.value, "id para is empty.", transid, status=4)

    if not personnum.strip():
        logger.warning("imgtype para is empty.")
        personnum = "No00000001"

    if "" == cnname.strip() and "" == enname.strip():
        logger.error("cnname and enname para all empty.")
        logger.info("transid:" + transid + ")face image input end.")
        return init_image_return(ERROR_CODE.FD_RECOG_ERROR_NAME_EMPTY.value, "cnname and enname para all empty.", transid, status=4)

    if "URL" == imagetype:
        if file is None:
            logger.error("image file para is empty.")
            logger.info("transid:" + transid + ")face image input end.")
            return init_image_return(ERROR_CODE.FD_RECOG_ERROR_FILE_NONE.value, "image file para is empty.", transid,
                                     status=4)

        if not file:
            logger.error("para(file) error.")
            logger.info("transid:" + transid + ")face image input end.")
            init_image_return(ERROR_CODE.FD_RECOG_ERROR_FILE_NONE.value, "para(file) error.", transid, status=3)

        img_file_name = file.filename
        # 对获得对图片校验
        result_split = img_file_name.split('.')
        if len(result_split) < 2:
            logger.error("image name format error.")
            logger.info("transid:" + transid + ")face image input end.")
            return init_image_return(ERROR_CODE.FD_RECOG_ERROR_IMAGE_FORMAT.value, "image name format error.", transid,
                                     status=4)

        # 获得图片文件的类型
        image_filetype = result_split[-1]
        if not image_type_check(image_filetype):
            logger.error("image type error.")
            logger.info("transid:" + transid + ")face image input end.")
            return init_image_return(ERROR_CODE.FD_RECOG_ERROR_IMAGE_TYPE.value, "image type error.", transid, status=4)

        # 保存原始图片文件
        contents = await file.read()
        write_file(SAVE_INIT_RAW_IMAGE_FILE_PATH + img_file_name, contents)

    elif "BASE64" == imagetype:
        img_file_name = transid + ".png"
        contents = base64.b64encode(file)
        write_file(SAVE_INIT_RAW_IMAGE_FILE_PATH + img_file_name, contents)

    else:
        logger.error("imagetype type error.")
        logger.info("transid:" + transid + ")face image input end.")

        return face_recog_return(ERROR_CODE.FD_CHK_ERROR_PICTYPE_ERROR.value, "imagetype para must be URL/BASE64.", transid, status=4)

    img_real_file = SAVE_INIT_RAW_IMAGE_FILE_PATH + img_file_name

    '''
    img = cv2.imread(img_real_file)
    
    # 人脸特征说明文件，计算机根据这些特征，进行人脸检测
    # 符合其中一部分特征，算作人脸
    face_detector = cv2.CascadeClassifier(FACE_CASCADE)

    # scaleFactor 缩放，默认 1.1   minNeighbors 默认 3，检测出来的人脸，就是坐标：x,y,w,h ，
    # faces = face_detector.detectMultiScale(img)
    # 检测黑白，显示彩色，数据变少提高效率
    gray = cv2.cvtColor(img, code=cv2.COLOR_BGR2GRAY)
    faces = face_detector.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=6, minSize=(25, 25))
    '''

    init_image = face_recognition.load_image_file(img_real_file)
    init_face_encoding = face_recognition.face_encodings(init_image)

    face_number = len(init_face_encoding)

    # 人脸的个数不是1时，说明图片不符合要求，必须是单人清晰正面照.
    if face_number != 1:
        logger.error("image guize error. face_number:"+str(face_number))
        logger.info("transid:" + transid + ")face image input end.")
        return init_image_return(ERROR_CODE.FD_RECOG_ERROR_IMAGE_TYPE.value, "图片不符合要求，必须是单人清晰正面照.", transid, status=5)

    # 人脸原始图片及个人信息持久化
    try:
        session = get_db()
        inner_channel_id = 11

        init_image_data = FaceRecogInitImage(
            is_deleted=0,
            personnel_number=personnum,
            cn_name=cnname,
            en_name=enname,
            raw_image_name=img_file_name,
            raw_image_path=img_real_file,
            from_channel=inner_channel_id,
            face_encoding=float_array_to_string(init_face_encoding[0]),
            reserve_int_1=0,
            reserve_char_1=''
        )
        session.add(init_image_data)
        session.commit()
        session.close()

    except ArithmeticError:
        logger.error("database except.")
        logger.info("transid:" + transid + ")face image input end.")
        return init_image_return(ERROR_CODE.FD_RECOG_ERROR_DB_EXCEPT, "database except.", transid, status=2)

    inner_trans_id = transid_generator(SERVICE_TYPE_IMAGE)

    result = {
        "requestId": transid,
        "responseId": inner_trans_id,
        "status": 1
    }
    return_result = response_result(ERROR_CODE.FD_RECOG_SUCCESS.value, "success", result)

    logger.info("face_detect success: ")
    logger.info(return_result)
    logger.info("transid:" + transid + ")face image input end.")
    return return_result


@face_recog_router.post("/image/recog")
async def imageRecognition(version: str, transid: str, imagetype: str, timestamp: str, nonce: str, secretid: str = None,
                       file: Union[UploadFile, str] = None) -> dict:
    """
    - author:
    - version: v1.0
    - title: 人脸识别-图片
    - description:
    - date:
    - param:
      - version: 本版号，此接口只支持v1
      - tansid: 任务id
      - imagetype: 图片类型：URL，BASE64
      - timestamp: 时间戳
      - nonce: 随机码，6位数字
      - secretid: 秘钥ID
      - file: 被识别的图片
    - return: JSON格式识别结果
    """

    logger.info("(transid:"+transid+")face recognition start.")
    logger.info("para:{" +
                "version:" + version +
                ", transid:" + transid +
                ", imagetype:" + imagetype +
                ", timestamp:" + timestamp +
                ", nonce:" + nonce + "}")

    # 接口版本号校验
    if not version_check(version):
        logger.error("version para error")
        logger.info("transid:" + transid + ")face recognition end.")
        return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_VAR_NO_SUPPORT.value, "version must be v1", transid, status=2)

    # 接口安全校验
    if secretid is not None and secretid.strip():
        if not timestamp.strip():
            logger.error("timestamp para is empty.")
            logger.info("transid:" + transid + ")face recognition end.")
            return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_TIMESTAMP_EMPTY.value, "timestamp para is empty.",
                                     transid, status=4)

        if not nonce.strip():
            logger.error("nonce para is empty.")
            logger.info("transid:" + transid + ")face recognition end.")
            return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_NONCE_EMPTY.value, "nonce para is empty.", transid,
                                     status=4)

        secret_raw = timestamp+nonce+SECRET_KEY
        secret_md5 = hashlib.md5(secret_raw.encode()).hexdigest()

        if secret_md5 != secretid:
            logger.error("secret check is failure.")
            logger.info("transid:" + transid + ")face recognition end.")
            return face_recog_return(ERROR_CODE.FD_RECOG_SECRETID_FAIL.value, "secretid check is failure.", transid, status=4)

    if not transid.strip():
        logger.error("transid para is empty.")
        logger.info("transid:" + transid + ")face recognition end.")
        return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_TRANSID_EMPTY.value, "id para is empty.", transid, status=4)

    if "URL" == imagetype:
        if file is None:
            logger.error("image file para is empty.")
            logger.info("transid:" + transid + ")face recognition end.")
            return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_FILE_NONE.value, "image file para is empty.", transid, status=4)

        if not file:
            logger.error("para(file) error.")
            logger.info("transid:" + transid + ")face recognition end.")
            face_recog_return(ERROR_CODE.FD_RECOG_ERROR_FILE_NONE.value, "image file para error.", transid, status=3)

        img_file_name = file.filename

        # 对获得对图片校验
        result_split = img_file_name.split('.')
        if len(result_split) < 2:
            logger.error("image name format error.")
            logger.info("transid:" + transid + ")face recognition end.")
            return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_IMAGE_FORMAT.value, "image name format error.", transid, status=4)

        # 获得图片文件的类型
        image_filetype = result_split[-1]

        if not image_type_check(image_filetype):
            logger.error("image type error.")
            logger.info("transid:" + transid + ")face recognition end.")
            return face_recog_return(ERROR_CODE.FD_RECOG_ERROR_IMAGE_TYPE.value, "image type error.", transid, status=4)

        # 保存原始图片文件
        contents = await file.read()
        write_file(SAVE_RAW_RECOG_IMAGE_FILE_PATH + img_file_name, contents)

    elif "BASE64" == imagetype:
        img_file_name = transid + ".png"
        contents = base64.b64encode(file)
        write_file(DATA_FILE_PATH + img_file_name, contents)
    else:
        logger.error("imagetype type error.")
        logger.info("transid:" + transid + ")face recognition end.")

        return face_recog_return(ERROR_CODE.FD_CHK_ERROR_PICTYPE_ERROR.value, "imagetype para must be URL/BASE64.", transid, status=4)

    img_real_file = SAVE_RAW_RECOG_IMAGE_FILE_PATH + img_file_name

    # 人脸识别逻辑
    recog_image = face_recognition.load_image_file(img_real_file)

    # 查找当前图片中的所有面和面编码
    recog_face_locations = face_recognition.face_locations(recog_image)
    recog_face_encodings = face_recognition.face_encodings(recog_image, recog_face_locations)

    face_number = len(recog_face_encodings)
    # 人脸的个数小于 1 报错返回
    if face_number < 1:
        logger.error("face recognition failure. transid:" + transid)
        logger.info("transid:" + transid + ")face recognition end.")
        return face_recog_return(ERROR_CODE.FD_RECOG_FAILURE.value, "face recognition failure", transid, status=2)

    # 当量不大时，通过查数据库获得所有图片
    try:
        session = get_db()
        all_init_image_items = query_all_init_image(session)
        session.close()

        if all_init_image_items is None:
            logger.error("face recognition failure. transid:" + transid)
            logger.info("transid:" + transid + ")face recognition end.")
            return response_result(ERROR_CODE.FD_GET_ERROR_IMAGE_NO_EXISTS.value, "image no exists.")

    except ArithmeticError:
        logger.error("query init image from db except, transId:" + transid)
        logger.info("transid:" + transid + ")face recognition end.")
        return response_result(ERROR_CODE.FD_GET_ERROR_DB_EXCEPT.value, "failure")

    # 创建已知人脸编码以及对应人名的数组
    known_face_encodings = []
    known_face_names = []

    for init_image_item in all_init_image_items:

        if "" != init_image_item.cn_name:
            known_face_name = init_image_item.cn_name
        else:
            known_face_name = init_image_item.en_name

        known_face_names.append(known_face_name)

        # known_face_image = face_recognition.load_image_file(init_image_item.raw_image_path)
        # known_face_encoding = face_recognition.face_encodings(known_face_image)[0]
        # 提高识别效率，人脸编码从数据库读取
        known_face_encoding = string_to_float_array(init_image_item.face_encoding)
        known_face_encodings.append(known_face_encoding)

    recog_face_image = face_recognition.load_image_file(img_real_file)

    # 查找当前图片中的所有面和面编码
    recog_face_locations = face_recognition.face_locations(recog_face_image)
    recog_face_encodings = face_recognition.face_encodings(recog_face_image, recog_face_locations)
    recog_face_names = []

    # MacOS 宋体字体文件
    the_font = ImageFont.truetype(FONT_FILE, 13)  # 加载字体, 字体大小

    for i in range(len(recog_face_encodings)):
        recog_encoding = recog_face_encodings[i]

        face_location = recog_face_locations[i]
        top, right, bottom, left = face_location
        cv2.rectangle(recog_face_image, (left, top), (right, bottom), (255, 0, 0), 1)

        # 在面下画一个有名字的标签
        cv2.rectangle(recog_face_image, (left, bottom - 16), (right, bottom), (255, 0, 0), cv2.FILLED)

        results = face_recognition.compare_faces(known_face_encodings, recog_encoding, tolerance=TOLERANCE)

        for j in range(len(results)):
            if results[j]:
                know_name = known_face_names[j]

                recog_face_names.append(know_name)

                img_pil = Image.fromarray(recog_face_image)
                draw = ImageDraw.Draw(img_pil)

                draw.text((left + 10, bottom - 16), know_name, font=the_font, fill=(0, 0, 0))  # xy坐标, 内容, 字体, 颜色

                recog_face_image = np.array(img_pil)

    recong_image_rgb = cv2.cvtColor(recog_face_image, cv2.COLOR_BGR2RGB)
    # 检测结果 文件持久化
    inner_trans_id = transid_generator(SERVICE_TYPE_IMAGE)

    target_file_name = inner_trans_id + '.' + image_filetype
    target_real_file = SAVE_RST_RECOG_IMAGE_FILE_PATH + target_file_name

    cv2.imwrite(target_real_file, recong_image_rgb)

    # 人脸识别记录入数据库表
    try:
        session = get_db()
        inner_channel_id = 11

        trans_data = Transaction(
            is_deleted=0,
            trans_id=inner_trans_id,
            user_id="U123456789",
            account_id="A0000000001",
            request_id=transid,
            from_channel=inner_channel_id,
            raw_file_name=img_file_name,
            file_type=11,
            raw_file_path=img_real_file,
            rst_file_name=target_file_name,
            rst_file_path=target_real_file,
            service_type=int(SERVICE_TYPE_IMAGE),
            fee_type=0,
            fail_reason='',
            status=4,
            reserve_int_1=1,
            reserve_char_1=''
        )
        session.add(trans_data)
        session.commit()
        session.close()

    except ArithmeticError:

        logger.error("query init image from db except, transid:" + transid)
        logger.info("transid:" + transid + ")face recognition end.")
        return response_result(ERROR_CODE.FD_GET_ERROR_DB_EXCEPT.value, "failure")

    # 先将数组类型编码成 jepg 类型的数据,然后转字节数组,最后将其用base64编码
    rtn, buf = cv2.imencode(".jpg", recong_image_rgb)
    dat = Image.fromarray(np.uint8(buf)).tobytes()
    recog_face_image_data = JPEG_HEADER + str(base64.b64encode(dat))[2:-1]
    recog_face_image_data_msg = {
        "img": recog_face_image_data
    }

    status = 1
    recog_face_number = len(recog_face_names)
    result = {
            "status": status,
            "requestId": transid,
            "responseId": inner_trans_id,
            "recogPersonSum": recog_face_number,
            "recogPersonName": recog_face_names,
            # "recogPersonNo": person_no,
            "recogResultImageData": recog_face_image_data_msg,
            "faceLocations": []
        }

    return_result = response_result(ERROR_CODE.FD_RECOG_SUCCESS.value, "success", result)

    logger.info("face recognition success: ")
#    logger.info(return_result)
    logger.info("transid:" + transid + ")face recognition end.")
    return return_result


@face_recog_router.get("/image/getrecogresult")
async def getDetectResult(version: str, transid: str, timestamp: str, nonce: str, secretid: str = None) -> dict:
    """
    - author:
    - version: v1.0
    - title: 查询人脸识别结果-图片
    - description:
    - date:
    - param:
      - version: 本版号，此接口只支持v1
      - tansid: 人脸识别时，SaaS服务平台返回给业务端的 responseId
      - timestamp: 时间戳
      - nonce: 随机码，6位数字
      - secretid: 秘钥ID
    - return: JSON格式识别结果
    """
    logger.info("(transid:" + transid + ")get face recognition result start.")

    logger.info("para:{" +
                "version:" + version +
                ", transid:" + transid +
                ", timestamp:" + timestamp +
                ", nonce:" + nonce + "}")

    # 接口版本号校验
    if not version_check(version):
        # 接口版本号错误
        logger.error("version para error")
        logger.info("(transid:" + transid + ")get face recognition result start.")
        return response_result(ERROR_CODE.FD_RECOG_ERROR_VAR_NO_SUPPORT.value, "version must be v1")

    # 接口安全校验
    if secretid is not None and secretid.strip():
        if not timestamp.strip():
            logger.error("timestamp para is empty.")
            logger.info("(transid:" + transid + ")get face recognition result end.")
            return response_result(ERROR_CODE.FD_GET_ERROR_TIMESTAMP_EMPTY.value, "timestamp para is empty.")

        if not nonce.strip():
            logger.error("nonce para is empty.")
            logger.info("(transid:" + transid + ")get face recognition result end.")
            return response_result(ERROR_CODE.FD_GET_ERROR_NONCE_EMPTY.value, "nonce para is empty.")

        secret_raw = timestamp+nonce+SECRET_KEY
        secret_md5 = hashlib.md5(secret_raw.encode()).hexdigest()

        if secret_md5 != secretid:
            logger.error("secret check is failure.")
            logger.info("(transid:" + transid + ")get face recognition result end.")
            return response_result(ERROR_CODE.FD_RECOG_SECRETID_FAIL.value, "secretid check is failure.")

    if not transid.strip():
        logger.error("transId is empty.")
        logger.info("(transid:" + transid + ")get face recognition result end.")
        return response_result(ERROR_CODE.FD_GET_ERROR_TRANSID_EMPTY.value, "transId para is empty.")

    # 通过 transid 查数据库获得图片文件名
    try:
        # 添加数据
        session = get_db()

        trans_item = get_trans_by_trans_id(session, transid)

        if trans_item is None:

            logger.error("image no exists, transId:"+transid)
            logger.info("(transid:" + transid + ")get face recognition result end.")
            return response_result(ERROR_CODE.FD_GET_ERROR_IMAGE_NO_EXISTS.value, "image no exists.")
        else:
            img_local_path = trans_item.rst_file_path

    except ArithmeticError:

        logger.error("find image path from db except, transId:" + transid)
        logger.info("(transid:" + transid + ")get face recognition result end.")
        return response_result(ERROR_CODE.FD_GET_ERROR_DB_EXCEPT.value, "image no exists.")

    # 校验数据返回的文件是否还存在
    if os.path.exists(img_local_path):

        # with open(img_local_path, 'rb') as img_file:
        #    img_stream_base64 = img_file.read()
        #    img_stream_base64 = base64.b64encode(img_stream_base64).decode()

        recog_face_image = face_recognition.load_image_file(img_local_path)

        # 先将数组类型编码成 jepg 类型的数据,然后转字节数组,最后将其用base64编码
        r, buf = cv2.imencode(".jpg", recog_face_image)
        dat = Image.fromarray(np.uint8(buf)).tobytes()
        recog_face_image_data = JPEG_HEADER + str(base64.b64encode(dat))[2:-1]
        recog_face_image_data_msg = {
            "img": recog_face_image_data
        }

        result = {
            "requestId": transid,
            "imageData": recog_face_image_data_msg
        }
        logger.info("face_detect success.")
        logger.info("(transid:" + transid + ")get face recognition result end.")
        # return render_template('./static/html/index.html', img_stream=img_stream_base64
        return response_result(ERROR_CODE.FD_GET_SUCCESS.value, "success", result)
    else:
        logger.error("image no exists:"+img_local_path)
        logger.info("(transid:" + transid + ")get face recognition result end.")
        return response_result(ERROR_CODE.FD_GET_ERROR_FILE_DELETED.value, "image no exists")

known_face_names = []
known_face_encodings = []


@face_recog_router.get("/capture/recog")
async def videoRecognitio(version: str, transid: str, timestamp: str, nonce: str, secretid: str = None, rtspurl = None):
    """
    - author:
    - version: v1.0
    - Title:
    - Description: 人脸识别-视频或摄像头
    - date:
    - param:
      - version:本版号，此接口只支持v1
      - transid: 任务id
      - timestamp: 时间戳
      - nonce: 随机码
      - secretid: 秘钥ID，6位数字
      - rtspurl: 如果videotype参数为video，此值为空，否则 TSP URL使用一个IP摄像头作为RTSP源，
                 比如："rtsp://admin:a2558404588@192.168.159.102/Streaming/Channels/2"
    - return: 将JPEG数据转换为字节字符串，并将其作为视频返回
    """

    global known_face_names
    global known_face_encodings

    logger.info("(transid:"+transid+")face recognition start.")
    logger.info("para:{" +
                "version:" + version +
                ", transid:" + transid +
                ", timestamp:" + timestamp +
                ", nonce:" + nonce +
                ", rtspurl:" + rtspurl + "}")
    # 接口版本号校验
    if not version_check(version):
        # 接口版本号错误
        logger.error("version para error")
        logger.info("transid:" + transid + ")face recognition end.")
        return response_result(ERROR_CODE.FD_RECOG_ERROR_VAR_NO_SUPPORT.value, "version must be v1")
    # 接口安全校验
    if secretid is not None and secretid.strip():
        if not timestamp.strip():
            logger.error("timestamp para is empty.")
            logger.info("transid:" + transid + ")face recognition end.")
            return response_result(ERROR_CODE.FD_RECOG_ERROR_TIMESTAMP_EMPTY.value, "timestamp para is empty.")

        if not nonce.strip():
            logger.error("nonce para is empty.")
            logger.info("transid:" + transid + ")face recognition end.")
            return response_result(ERROR_CODE.FD_RECOG_ERROR_NONCE_EMPTY.value, "nonce para is empty.")

        secret_raw = timestamp+nonce+SECRET_KEY
        secret_md5 = hashlib.md5(secret_raw.encode()).hexdigest()

        if secret_md5 != secretid:
            logger.error("secret check is failure.")
            logger.info("transid:" + transid + ")face recognition end.")
            return response_result(ERROR_CODE.FD_RECOG_SECRETID_FAIL.value, "secretid check is failure.")

    if not transid.strip():
        logger.error("transid para is empty.")
        logger.info("transid:" + transid + ")face recognition end.")
        return response_result(ERROR_CODE.FD_RECOG_ERROR_TRANSID_EMPTY.value, "id para is empty.")

    # 当量不大时，通过查数据库获得所有图片
    try:
        session = get_db()
        all_init_image_items = query_all_init_image(session)

        if all_init_image_items is None:
            logger.error("face recognition failure. transid:" + transid)
            logger.info("transid:" + transid + ")face recognition end.")
            return response_result(ERROR_CODE.FD_GET_ERROR_IMAGE_NO_EXISTS.value, "image no exists.")

    except ArithmeticError:
        logger.error("query init image from db except, transId:" + transid)
        logger.info("transid:" + transid + ")face recognition end.")
        return response_result(ERROR_CODE.FD_GET_ERROR_DB_EXCEPT.value, "failure")

    # 创建已知人脸编码以及对应人名的数组
    for init_image_item in all_init_image_items:

        if "" != init_image_item.cn_name:
            known_face_name = init_image_item.cn_name
        else:
            known_face_name = init_image_item.en_name

        known_face_names.append(known_face_name)
        # known_face_image = face_recognition.load_image_file(init_image_item.raw_image_path)
        # known_face_encoding = face_recognition.face_encodings(known_face_image)[0]

        # 提高识别效率，人脸编码从数据库读取
        known_face_encoding = string_to_float_array(init_image_item.face_encoding)

        known_face_encodings.append(known_face_encoding)

    return StreamingResponse(generate_frames_recog(rtspurl), media_type="multipart/x-mixed-replace;boundary=frame")

    #html = "<html><head><title>摄像视频人脸识别 SaaS</title> </head><body><img src='./face_recog_video_feed' width='70%' height='70%'></body></html>"
    #return Response(content=html, media_type="text/html")

process_this_frame = 10


# 定义一个辅助函数，用于从RTSP源读取视频流。
def generate_frames_recog(rtspurl=""):

    global known_face_names
    global known_face_encodings
    global process_this_frame

    face_locations = []
    frame_face_names = []
    # MacOS 宋体字体文件
    the_font = ImageFont.truetype(FONT_FILE, 13)  # 加载字体, 字体大小

    # 开启一个摄像头
    if not rtspurl.strip():
        cap = cv2.VideoCapture(0)
    else:
        cap = cv2.VideoCapture(rtspurl)
    if cap is None:
        return None
    while True:
        if 0 == process_this_frame:
            process_this_frame = 10

        # 从视频流中读取帧
        # 从摄像头读取数据, 读取成功 ret为True,否则为False,frame里面就是一个三维数组保存图像
        ret_cap, frame = cap.read()

        if not ret_cap:
            break

        # 将视频帧的大小调整为1/4以加快人脸识别处理
        small_frame = cv2.resize(frame, (0, 0), fx=SETSIZE, fy=SETSIZE)

        # 将图像从BGR颜色（OpenCV使用）转换为RGB颜色（人脸识别使用）
        rgb_small_frame = np.ascontiguousarray(small_frame[:, :, ::-1])

        # process_this_frame = True

        # 仅每隔一帧处理一次视频以节省时间

        # if process_this_frame:
        if process_this_frame:

            # 查找当前视频帧中的所有面和面编码
            face_locations = face_recognition.face_locations(rgb_small_frame)
            face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

            frame_face_names = []
            for face_encoding in face_encodings:

                # 查看该面是否与已知面匹配
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding, tolerance=TOLERANCE)

                # 如果在已知的人脸编码中找到匹配项，请使用第一个。
                # 如果匹配为True：
                # first_match_index=matches.index(true)
                # name=known_face_names[first_match_index]

                # 或者，使用与新面的距离最小的已知面
                face_distances = face_recognition.face_distance(known_face_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)

                if matches[best_match_index]:
                    name = known_face_names[best_match_index]
                elif process_this_frame % 2:
                    name = "？？？？"
                else:
                    name = "未知"

                frame_face_names.append(name)

        # process_this_frame = not process_this_frame
        process_this_frame = process_this_frame - 1
        # 显示结果
        for (top, right, bottom, left), name in zip(face_locations, frame_face_names):
            # 由于我们在中检测到的帧被缩放到1/4大小，因此缩放备份面位置
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4

            # 在脸上画一个方框
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # 在面下画一个有名字的标签
            cv2.rectangle(frame, (left, bottom - 25), (right, bottom), (0, 0, 255), cv2.FILLED)
            # font = cv2.FONT_HERSHEY_DUPLEX
            # cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

            img_pil = Image.fromarray(frame)
            draw = ImageDraw.Draw(img_pil)
            draw.text((left + 18, bottom - 25), name, font=the_font, fill=(0, 0, 0))  # xy坐标, 内容, 字体, 颜色
            frame = np.array(img_pil)

        # 先将数组类型编码成 jepg 类型的数据,然后转字节数组,最后将其用base64编码
        ret1, buffer = cv2.imencode(".jpg", frame)

        # dat = Image.fromarray(np.uint8(buffer)).tobytes()
        # img_date_url = JPEG_HEADER + str(base64.b64encode(dat))[2:-1]

        # 将JPEG数据转换为字节字符串，并将其作为流响应返回。
        yield (b"--frame\r\n"
               b"Content-Type: image/jpeg\r\n\r\n" + buffer.tobytes() + b"\r\n")

        time.sleep(0.25)
    # 释放视频流。
    cap.release()


'''
# 定义一个FastAPI路由，用于处理视频流请求。
@face_recog_router.get("/video/face_recog_video_feed")
async def face_recog_video_feed():
    # 将视频流作为流响应返回。
    return StreamingResponse(generate_frames_recog(), media_type="multipart/x-mixed-replace;boundary=frame")
'''
