import os
import pymysql

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sshtunnel import SSHTunnelForwarder

import configparser

# -从配置文件获取数据库配置信息
# --获取当前文件目录
current_path = os.path.abspath(os.path.dirname(__file__))

# --获取SaaS服务根目录
saas_root_path = current_path[:current_path.rindex('db')]

# --数据库配置文件以及数据操作相关配置项（Linux/macOS）
CONF_DB_FILE = saas_root_path + '/conf/saas_setting.conf'

conf = configparser.ConfigParser()
conf.read(CONF_DB_FILE, encoding='gbk')

ssh = (conf.get("mysql", 'ssh'))

ssh_host = conf.get("mysql", 'ssh_host')
ssh_port = int(conf.get("mysql", 'ssh_port'))
ssh_username = conf.get("mysql", 'ssh_username')
ssh_password = conf.get("mysql", 'ssh_password')

mysql_host = conf.get("mysql", 'mysql_host')
mysql_port = conf.get("mysql", 'mysql_port')

mysql_username = conf.get("mysql", 'mysql_username')
mysql_password = conf.get("mysql", 'mysql_password')

mysql_db = conf.get('mysql', 'mysql_database')
mysql_charset = conf.get("mysql", 'mysql_charset')

local_port = mysql_port

# -数据库连接及创建数据库会话
# --连接mysql数据库
SQLALCHEMY_DATABASE_URL = ""
pymysql.install_as_MySQLdb()
if "YES" == ssh:
    server = SSHTunnelForwarder(
        ssh_address_or_host=(ssh_host, ssh_port),
        ssh_username=ssh_username,
        ssh_password=ssh_password,
        remote_bind_address=(mysql_host, int(mysql_port))
        )
    server.start()
    SQLALCHEMY_DATABASE_URL = "mysql+pymysql://" + mysql_username + ":" + mysql_password + "@" + mysql_host + ":" + str(server.local_bind_port) + "/" + mysql_db
elif "NO" == ssh:
    SQLALCHEMY_DATABASE_URL = "mysql://" + mysql_username + ":" + mysql_password + "@" + mysql_host + ":" + mysql_port + "/" + mysql_db

# --配置数据库地址：数据库类型+数据库驱动名称://用户名:密码@机器地址:端口号/数据库名


# --创建 SQLAlchemy 引擎
engine = create_engine(SQLALCHEMY_DATABASE_URL)

# --创建数据库会话
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

# --创建一个Base类declarative_base，以备创建每个数据库模型或类（ORM 模型
Base = declarative_base()


def get_db():
    # 每个请求有一个独立的数据库会话/连接（SessionLocal），在所有请求中使用相同的会话，然后在请求完成后关闭它。
    db = SessionLocal()
    return db
