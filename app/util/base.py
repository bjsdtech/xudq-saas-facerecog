import numpy

def float_array_to_string(float_array):
    # 将numpy array类型转化为列表
    float_list = float_array.tolist()
    # 将列表里的元素转化为字符串
    string_list = [str(i) for i in float_list]
    # 拼接列表里的字符串
    string = ','.join(string_list)
    return string

def string_to_float_array(string):
    # 将字符串转为numpy 转换成一个list
    string_list = string.strip(' ').split(',')
    # 将list中str转换为float
    float_list = list(map(float, string_list))
    float_array = numpy.array(float_list)

    return float_array