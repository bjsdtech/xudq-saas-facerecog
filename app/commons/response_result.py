def response_result(code, msg, result={}):
    return {
        "code": code,
        "msg": msg,
        "result": result
    }


def init_image_return(code, msg, transid, status, responseid=''):

    return {
        "code": code,
        "msg": msg,
        "result": {
            "requestId": transid,
            "responseId": responseid,
            "status": status
        }
    }


def face_recog_return(code, msg, trans_id, status, responseid='', recogsum=0, facenames=[], imagedatamsg={}):

    return {
        "code": code,
        "msg": msg,
        "result": {
            "requestId": trans_id,
            "responseId": responseid,
            "recogSum": recogsum,
            "recogPersonName": facenames,
            # "recogPersonNo": person_no,
            "recogResultFile": imagedatamsg,
            "status": status
        }
    }
