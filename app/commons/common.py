import datetime
import random

SUPPORT_IMAGE_TYPE = ['jpg', 'png', 'jpeg']

# 11-图片人脸检测，12-视频人脸检测，13-摄像头人脸检测，14-音频检测，21-图片人脸识别，22-视频人脸识别，23-摄像头人脸识别，31-证照OCR，32-票证OCR，
SUPPORT_SERVICE_TYPE = ['11', '12', '13', '14', '21', '22', '23', '31', '32']


def write_file(filename, data):
    with open(filename, 'wb') as wfile:
        wfile.write(data)


def image_type_check(filetype: str):
    if filetype not in SUPPORT_IMAGE_TYPE:
        return False
    else:
        return True


def transid_generator(servicetype: str):

    current_time = datetime.datetime.now().strftime('%Y%m%d%H%M%S')
    if servicetype not in SUPPORT_SERVICE_TYPE:
        servicetype = '00'

    new_trans_id = "T"+servicetype+current_time + str(random.randint(100000, 999999))

    return new_trans_id
