# coding: utf-8
import os
from loguru import logger
import configparser

from datetime import datetime

# -从配置文件获取数据库配置信息
# --获取当前文件目录
current_path = os.path.abspath(os.path.dirname(__file__))

# --获取SaaS服务根目录
saas_root_path = current_path[:current_path.rindex('commons')]

# --数据库配置文件以及数据操作相关配置项（Linux/macOS）
CONF_DB_FILE = saas_root_path + '/conf/saas_setting.conf'

conf = configparser.ConfigParser()
conf.read(CONF_DB_FILE, encoding='gbk')

log_path = conf.get('logger', 'path')
dateformat = conf.get('logger', 'format')

rotation = conf.get('logger', 'rotation')
retention = conf.get('logger', 'retention')
enqueue = conf.get('logger', 'enqueue')

# -创建日志信息
if not os.path.exists(log_path):
    os.mkdir(log_path)

log_file = '{0}/facedetect-{1}.log'.format(log_path, datetime.now().strftime(dateformat))

logger.add(log_file, rotation=rotation, retention=retention, enqueue=enqueue)
# coding: utf-8
import os
from loguru import logger
import configparser

from datetime import datetime

# -从配置文件获取数据库配置信息
# --获取当前文件目录
current_path = os.path.abspath(os.path.dirname(__file__))

# --获取SaaS服务根目录
saas_root_path = current_path[:current_path.rindex('commons')]

# --数据库配置文件以及数据操作相关配置项（Linux/macOS）
CONF_DB_FILE = saas_root_path + '/conf/saas_setting.conf'

conf = configparser.ConfigParser()
conf.read(CONF_DB_FILE, encoding='gbk')

log_path = conf.get('logger', 'path')
dateformat = conf.get('logger', 'format')

rotation = conf.get('logger', 'rotation')
retention = conf.get('logger', 'retention')
enqueue = conf.get('logger', 'enqueue')

# -创建日志信息
if not os.path.exists(log_path):
    os.mkdir(log_path)

log_file = '{0}/facerecog-{1}.log'.format(log_path, datetime.now().strftime(dateformat))

logger.add(log_file, rotation=rotation, retention=retention, enqueue=enqueue)
